---
maturity: Ready for Production Test
---

# Modbus RiG-ePlus eCO2 TVOC module CSS811 with BME280 w Arduino Pro Mini

RS485 modbus sensor module with CJMCU-811 and BME280 module Aliexpress

## CSS-811


Gas Sensor Carbon Dioxide Detection Sensor Module CCS811 CO2 eCO2 TVOC Air Quality Detecting I2C Output CJMCU-811

|CCS811|BME280|
|---|---|
|![](img/CCS-811.png)|![](img/BME280.jpg)|

### Features CCS811

CCS-811 air quality monitoring Digital Internal gas sensor,the CJMCU-811 is an ultra-low-power digital gas sensor that integrates a sensor and a CCS801-bit MCU with an analog-digital converter (ADC) for detecting indoor air quality, including carbon monoxide (CO) and a wide range of Volatile Organic compounds (VOCs).
air quality sensor CCS811 characteristics:

- Ultra-low power consumption can be used in battery powered equipment
- high sensitivity, fast heating
- Intelligent algorithm calculates TVOC / eCO2 values
- I2C signal output, direct communication with the main system

the main characteristics of the air quality sensor CJMCU-811:
- metal oxide (MOX) sensors to monitor indoor air quality
- a-bit MCU is integrated to perform the \'first-level algorithm
- adc built in for sensor readings and digitized conversions
- I2C slave interface can be connected directly to the master control system
- Reset / interrupt control

### BME280


![](img/BME280-leaf.jpg)

| Key features            |  values                                                                          |
|-----------------------|:----------------------------------------------------------------------------------|
|  Digital interface  | SPI (3 and 4 wire, up to 10 MHz)                                                     |
|  Supply voltage     | VDD main supply voltage range: 1.71 V to 3.6 V<br> VDDIO interface voltage range: 1.2 V to 3.6 V  |
|  Current consumption | 3.6 μA @ 1 Hz humidity, pressure and temperature                                    |
|  Operating rang| -40...+85 °C, 0...100 % rel. humidity, 300...1100 hPa                                     |


|Key parameters for humidity sensor ||
|---|:---|
|  Response time            | 1s                       |
|  Accuracy tolerance      |   ±3 % relative humidity |
|  Hysteresis              |  ±1% relative humidity   |

| Key parameters for pressure sensor   |     |
|---|:---|
| RMS Noise | 0.2 Pa, equiv. to 1.7 cm                                                                    |
| Offset temperature coefficient | ±1.5 Pa/K, equiv. to ±12.6 cm at 1 °C temperature change               |

#### Applications

- Context awareness, e.g. skin detection, room change detection
-  Health monitoring / well-being
    - Warning regarding dehydration or heat stroke
    - Spirometry (measurement of lung volume and air flow)
- Home automation control
    - control heating, venting, air conditioning (HVAC)
- Internet of things
- GPS enhancement (e.g. time-to-first-fix improvement, dead reckoning, slope detection)
- Indoor navigation (change of floor detection, elevator detection)
- Outdoor navigation, leisure and sports applications
- Weather forecast
- Vertical velocity indication (rise/sink speed)

### Connection CJMCU-811 IIC

| Ardu | pin | CJMCU-811 | pin | COLOR|
|---|---|---|---|---|
| 3V3  |  VCC | VCC | 1 | RED |
| GND  |  GND | GND | 2 | Black |
| SCL  |  A5 | SCL | 3 | YELLOW |
| SDA  |  A4 | SDA | 4 | WHITE | 
| D4  |  4 | WAKE | 5 |  |

#### Unused:


| CJMCU-811 | pin | function |Type|
|---|---|---|---|
| INT   | 6 | interrupt line | Output, Open Collector|
| RESET | 7 | active LOW | Input, Pull Up |
| ADD   | 8 | I@C Address: open,GND=0x5a, VCC=0x5b | Input, Pull Down |


### Connection BME280 3v3 SPI

| Ardu | pin | BME280 | pin | COLOR|
|---|---|---|---|---|
| 3V3  |  VCC | VCC | 1 | BLACK |
| GND  |  GND | GND | 2 | RED |
| SCK  |   13 | SCL | 3 | WHITE |
| MISO |   12 | SDO | 6 | GREEN | 
| MOSI |   11 | SDA | 4 | YELLOW |
| CS   |   10 | CSB | 5 | ORANGE |

### Connection RS485

| Ardu | pin | RS485 | pin | COLOR|
|---|---|---|---|---|
| 3V3  |  VCC | VCC | 4 | BLACK |
| GND  |  GND | GND | 1 | RED |
|  RXD |   RX1 | RXD | 3 | YELLOW |
|  TXD |   TX0 | TXD | 2 | White |

![](img/RS485_power.jpg)

### Connection POWER

| Ardu | pin | POWER | pin | COLOR|
|---|---|---|---|---|
| 5V  |  RAW | 5V | + | RED |
| GND |  GND | GND | - | BLACK |

![](img/schematic.png)

### Partlist

**73-297** RiG-ePlus

| Line | SeQ | Description                                    | Quantity | UNIT | PartNumber | Unit Cost [USD] | Cost[USD] |
|------|-----|------------------------------------------------|----------|------|------------|-----------------|-----------|
| 1    | **0**   | **RiG-ePlus RS485 Modbus Unit**            |          |      | **73-297** | **23.99**       |  <---     |
| 2    | 1   | Wall terminal 2EDGWB-5.08-5P                   | 1        | pce  | 73-259     | 2,83            | 2.83      |
| 3    | 1   | Module TTL-RS485-3V3                           | 1        | pce  | 73-1884    | 0.50            | 0.50      |
| 4    | 1   | Arduino Pro Mini 3v3 8MHz                      | 1        | pce  | 63-3361    | 3.00            | 3.00      |
| 5    | 1   | Header connector 40P (6P)                      | 0.15     | pce  | 62-548     | 0.05            | 0.01      |
| 6    | 1   | MODULE MICRO 5V/2A STEP DOWN                   | 1        | pce  | 73-1886    | 1.00            | 1.00      |
| 7    | 1   | BOX BASE RiG-ePLUS BLACK                       | 1        | pce  | 73-298     |                 |           |
| 8    | 2   | ABS+                                           | 0.05     | kg   | 66-883     | 18.00           | 0.90      |
| 9    | 2   | electric power                                 | 1        | KWh  | na         | 0.16            | 0.16      |
| 10   | 1   | BOX COVER RiG-ePLUS BLACK                      | 1        | pce  | 73-299     |                 |           |
| 11   | 2   | ABS+                                           | 0.05     | kg   | 66-883     | 18.00           | 0.90      |
| 12   | 2   | electric power                                 | 1        | KWh  | na         | 0.16            | 0.16      |
| 13   | 1   | Wire Cable Connector 5P JST XH2.54 XH 15cm set | 1        | pce  | 62-282     | 0.20            | 0.20      |
| 14   | 1   | Wire Cable Connector 6P JST XH2.54 XH 15cm set | 1        | pce  | 62-3373    | 0.25            | 0.25      |
| 15   | 1   | Wire Cable Connector 4P JST XH2.54 XH 15cm set | 1        | pce  | 73-267     | 0.15            | 0.15      |
| 16   | 1   | Wire Cable Connector 2P JST XH2.54 XH 15cm set | 1        | pce  | 73-269     | 0.10            | 0.10      |
| 17   | 1   | MODULE BME280 3V3 SPI, PTH                     | 1        | pce  | 73-263     | 8.87            | 8.87      |
| 18   | 1   | MODULE CJMCU-811 3v3 I2C CO2-TVOC SENSOR       | 1        | pce  | 73-296     | 5.08            | 5.08      |
| 19   | 1   | Fiber 3mm                                      | 0.015    | m    |            | 3.00            | 0.05      |

## RiG-ePlus with BME280 Arduino Pro Mini (MODBUS RTU)

UnitId : 160..199 , set by change the modbus address, see FC06 [change unitId](#preset-single-register-fc06), with code from project [change unitid modbus](https://gitlab.com/igcgroup/logit/change-unitid-modbus)


### Force Single Coil (FC05)

write address 229 starts the mesurment cycle

### Preset Single Register (FC06)

**change unitId**:

write address 9191 `<new unitID>` will update the unitId to the new value and reset the unit.


### Read Holding Registers (FC03)

Used for debug or check purpose

READ FC03 from address `9191`, returns 16bit integer value of `9191`.

### Read Input Registers (FC04)

*READY after Broadcast adress 229*

Data is in 32bit float value's:

| Add | Function |
|---|---|
| 0 | Temperature TC*100 [`Degree Celcius`] |
| 1 | Humidity TC*100 [%]|
| 2,3 | Presure [Pa] (Float32)|
| 4 | eCO2 [ppm] uint16 |
| 5 | TVOC [ppb] uint16 |
| 6 | CCS811 STATUS **`*(2)`**|
| 7 | MODBUS Status **`*(1)`**|

#### MODBUS `*(1)`

status of the internal modbus ( FC5 => 229 / FC4 )

| Bit | Description |
|-----|---------------------------|
| 0 | MOD_STATUS_IDLE |
| 1 | MOD_STATUS_RDY |
| 2 | MOD_STATUS_ERROR |

#### CSS811 STATUS `*(2)`
| Error | Value| Description|
|---|---|------------------|
| CCS811_ERRSTAT_ERROR              | 0x0001 | There is an error, the ERROR_ID register (0xE0) contains the error source |
| CCS811_ERRSTAT_I2CFAIL            | 0x0002 | Bit flag added by software: I2C transaction error |
| CCS811_ERRSTAT_DATA_READY         | 0x0008 | A new data sample is ready in ALG_RESULT_DATA |
| CCS811_ERRSTAT_APP_VALID          | 0x0010 | Valid application firmware loaded |
| CCS811_ERRSTAT_FW_MODE            | 0x0080 | Firmware is in application mode (not boot mode) |
| CCS811_ERRSTAT_WRITE_REG_INVALID  | 0x0100 | The CCS811 received an I²C write request addressed to this station but with invalid register address ID |
| CCS811_ERRSTAT_READ_REG_INVALID   | 0x0200 | The CCS811 received an I²C read request to a mailbox ID that is invalid |
| CCS811_ERRSTAT_MEASMODE_INVALID   | 0x0400 | The CCS811 received an I²C request to write an unsupported mode to MEAS_MODE |
| CCS811_ERRSTAT_MAX_RESISTANCE     | 0x0800 | The sensor resistance measurement has reached or exceeded the maximum range |
| CCS811_ERRSTAT_HEATER_FAULT       | 0x1000 | The heater current in the CCS811 is not in range |
| CCS811_ERRSTAT_HEATER_SUPPLY      | 0x2000 | The heater voltage is not being applied correctly |

### Data extraction

Read FC04,  Address 0, 8 registers.

```java
var rawData = new ArrayBuffer(4);
var intView = new Uint16Array(rawData);
var flt2View = new Float32Array(rawData);

intView[0] = msg.payload[2+1]; //lower 16 bit Presure Float32
intView[1] = msg.payload[2];   //higher 16 bit Presure Float32


fields= { temperature:0.0,humidity:0.0,presure:0.0,eco2:400,tvoc:0, status_CCS811: 0,error: false};

//set the fields
fields.temperature=parseFloat((msg.payload[0]/100.0).toFixed(1));
fields.humidity=parseFloat((msg.payload[1]/100.0).toFixed(1));
fields.presure=parseFloat((flt2View[0]/100.0).toFixed(2));
fields.eco2=msg.payload[4];
fields.tvoc=msg.payload[5];
fields.status_CCS811=msg.payload[6];
fields.error=((msg.payload[7] & 0x0004) == 0x0004)

msg.payload={ bucket: 'logit', precision: 'ms', 
        data: [{ measurement:"POL5:SCALE:ePlus", tags:{location:"POL5-SCALE", function:"TPHCVR" },
                 fields: {},
                 timestamp:{}} ] };

msg.payload.data[0].fields=fields;


msg.payload.data[0].timestamp=global.get("timestamp");

msg.topic = "temp";
node.status({fill:"red",shape:"ring",text:msg.topic + ":" + msg.payload.data[0].fields.temperature});   

return msg;

```


### test nodered

![Dash ePlus](img/Dash_ePlus.png)

![Grapical Code nodered](img/nodered.png)


```json
[
    {
        "id": "9948466a.d6ebc8",
        "type": "tab",
        "label": "Broadcast",
        "disabled": false,
        "info": ""
    },
    {
        "id": "8d434d04.0bb158",
        "type": "ui_artlessgauge",
        "z": "9948466a.d6ebc8",
        "group": "b428ba8.81494c8",
        "order": 1,
        "width": 4,
        "height": 4,
        "name": "Temp polr scale",
        "icon": "fa-thermometer-quarter",
        "label": "Temperature POL5-scale",
        "unit": "Degree",
        "layout": "radial",
        "decimals": "1",
        "differential": false,
        "minmax": false,
        "colorTrack": "#555555",
        "colorFromTheme": true,
        "property": "payload.data[0]fields.temperature",
        "sectors": [
            {
                "val": 20,
                "col": "#ff0033",
                "t": "min",
                "dot": 3
            },
            {
                "val": 30,
                "col": "#ff0033",
                "t": "max",
                "dot": 10
            }
        ],
        "lineWidth": 3,
        "bgcolorFromTheme": true,
        "diffCenter": "",
        "x": 900,
        "y": 500,
        "wires": []
    },
    {
        "id": "fa018e28.687f88",
        "type": "ui_artlessgauge",
        "z": "9948466a.d6ebc8",
        "group": "b428ba8.81494c8",
        "order": 3,
        "width": 4,
        "height": 4,
        "name": "Humidity",
        "icon": "fa-tint ",
        "label": "Humidity",
        "unit": "%",
        "layout": "radial",
        "decimals": "1",
        "differential": false,
        "minmax": true,
        "colorTrack": "#555555",
        "colorFromTheme": true,
        "property": "payload.data[0]fields.humidity",
        "sectors": [
            {
                "val": 0,
                "col": "#002aff",
                "t": "min",
                "dot": 0
            },
            {
                "val": 100,
                "col": "#002aff",
                "t": "max",
                "dot": 0
            }
        ],
        "lineWidth": 3,
        "bgcolorFromTheme": true,
        "diffCenter": "",
        "x": 880,
        "y": 540,
        "wires": []
    },
    {
        "id": "2933ed52.336012",
        "type": "ui_artlessgauge",
        "z": "9948466a.d6ebc8",
        "group": "b428ba8.81494c8",
        "order": 2,
        "width": 4,
        "height": 4,
        "name": "Presure",
        "icon": "fa-themeisle",
        "label": "Barometric Presure",
        "unit": "hPa",
        "layout": "radial",
        "decimals": "0",
        "differential": false,
        "minmax": true,
        "colorTrack": "#555555",
        "colorFromTheme": true,
        "property": "payload.data[0]fields.presure",
        "sectors": [
            {
                "val": 950,
                "col": "#ff9900",
                "t": "min",
                "dot": 0
            },
            {
                "val": 1100,
                "col": "#ff9900",
                "t": "max",
                "dot": 0
            }
        ],
        "lineWidth": 3,
        "bgcolorFromTheme": true,
        "diffCenter": "",
        "x": 880,
        "y": 580,
        "wires": []
    },
    {
        "id": "61819ea4.2d53",
        "type": "function",
        "z": "9948466a.d6ebc8",
        "name": "RiG-ePlus @location",
        "func": "\n    var rawData = new ArrayBuffer(4);\n    var intView = new Uint16Array(rawData);\n    var flt2View = new Float32Array(rawData);\n    intView[0] = msg.payload[2+1]; //low\n    intView[1] = msg.payload[2]; //high\n\n\n\nfields= { temperature:0.0,humidity:0.0,presure:0.0,eco2:400,tvoc:0, status_CCS811: 0,error: false};\n\n//set fields\nfields.temperature=parseFloat((msg.payload[0]/100.0).toFixed(1));\nfields.humidity=parseFloat((msg.payload[1]/100.0).toFixed(1));\nfields.presure=parseFloat((flt2View[0]/100.0).toFixed(2));\nfields.eco2=msg.payload[4];\nfields.tvoc=msg.payload[5];\nfields.status_CCS811=msg.payload[6];\nfields.error=((msg.payload[7] & 0x0004) == 0x0004)\n\nmsg.payload={ bucket: 'logit', precision: 'ms', data: [{ measurement:\"LOCATION:ePlus\", tags:{location:\"HASH-LOCATION\", function:\"TPHCVR\" },fields: {},timestamp:{}} ] };\n\nmsg.payload.data[0].fields=fields;\n\n\nmsg.payload.data[0].timestamp=global.get(\"timestamp\");\n\nmsg.topic = \"temp\";\nnode.status({fill:\"red\",shape:\"ring\",text:msg.topic + \":\" + msg.payload.data[0].fields.temperature});   \n\nreturn msg;\n",
        "outputs": 1,
        "noerr": 0,
        "x": 620,
        "y": 500,
        "wires": [
            [
                "8d434d04.0bb158",
                "fa018e28.687f88",
                "2933ed52.336012",
                "feac783d.28bd78",
                "851f0d3c.a6e6c8"
            ]
        ]
    },
    {
        "id": "9056465b.843d6",
        "type": "ui_template",
        "z": "9948466a.d6ebc8",
        "group": "b428ba8.81494c8",
        "name": "Flash",
        "order": 6,
        "width": 4,
        "height": 4,
        "format": "<div style=\"display:flex; overflow: hidden;tex-align:left\">\n<p><svg height=\"100\" Width=\"100\" viewBox=\"0 0 48 48\">\n    <polygon fill=\"{{msg.payload}}\" points=\"33,22 23.6,22 30,5 19,5 13,26 21.6,26 17,45\"/>\n</svg>\n</p>\n</div>",
        "storeOutMessages": true,
        "fwdInMessages": true,
        "resendOnRefresh": true,
        "templateScope": "local",
        "x": 870,
        "y": 380,
        "wires": [
            []
        ]
    },
    {
        "id": "bfc148b7.c56998",
        "type": "link in",
        "z": "9948466a.d6ebc8",
        "name": "FLASH",
        "links": [
            "4b787148.ac93c8"
        ],
        "x": 690,
        "y": 420,
        "wires": [
            [
                "9056465b.843d6"
            ]
        ],
        "icon": "node-red/arrow-in.svg",
        "l": true
    },
    {
        "id": "8ccfd01.a10a3b",
        "type": "function",
        "z": "9948466a.d6ebc8",
        "name": "prep flash",
        "func": "var newmsg={};\n\nif (msg.topic==\"init\") //not real value\n{\n     newmsg.payload= \"none\" ;\n\treturn newmsg;\n\n}\n\nif (msg.payload== \"none\")\nreturn msg;\nelse\n{\n\n\tnewmsg = {\n    payload: \"lime\" \n\t};\n\treturn newmsg;\n}",
        "outputs": 1,
        "noerr": 0,
        "x": 680,
        "y": 380,
        "wires": [
            [
                "9056465b.843d6"
            ]
        ]
    },
    {
        "id": "a01b2ea1.ab41d8",
        "type": "change",
        "z": "9948466a.d6ebc8",
        "name": "Clear it",
        "rules": [
            {
                "t": "set",
                "p": "payload",
                "pt": "msg",
                "to": "none",
                "tot": "str"
            }
        ],
        "action": "",
        "property": "",
        "from": "",
        "to": "",
        "reg": false,
        "x": 520,
        "y": 380,
        "wires": [
            [
                "8ccfd01.a10a3b"
            ]
        ]
    },
    {
        "id": "18ae0958.755467",
        "type": "delay",
        "z": "9948466a.d6ebc8",
        "name": "",
        "pauseType": "delay",
        "timeout": "1",
        "timeoutUnits": "seconds",
        "rate": "1",
        "nbRateUnits": "1",
        "rateUnits": "second",
        "randomFirst": "1",
        "randomLast": "5",
        "randomUnits": "seconds",
        "drop": false,
        "x": 340,
        "y": 380,
        "wires": [
            [
                "a01b2ea1.ab41d8"
            ]
        ]
    },
    {
        "id": "ffc362e9.2802f8",
        "type": "modbus-flex-write",
        "z": "9948466a.d6ebc8",
        "name": "Write Broadcast",
        "showStatusActivities": true,
        "showErrors": true,
        "server": "b24a2e1a.d69b",
        "emptyMsgOnFail": false,
        "keepMsgProperties": false,
        "x": 660,
        "y": 160,
        "wires": [
            [
                "1fb67b7.0337e05",
                "88236e.6131d49"
            ],
            []
        ]
    },
    {
        "id": "b0c8612f.988fb",
        "type": "function",
        "z": "9948466a.d6ebc8",
        "name": "Broadcast Start Measure Data",
        "func": "\nmsg.payload = { value: true , 'fc': 5, 'unitid': 0, 'address': 229 , 'quantity': 1 }; \nreturn msg;",
        "outputs": 1,
        "noerr": 0,
        "x": 390,
        "y": 160,
        "wires": [
            [
                "ffc362e9.2802f8",
                "10b18295.20351d"
            ]
        ]
    },
    {
        "id": "39c53082.d8ca",
        "type": "inject",
        "z": "9948466a.d6ebc8",
        "name": "Broadcast",
        "topic": "",
        "payload": "",
        "payloadType": "date",
        "repeat": "5",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 150,
        "y": 160,
        "wires": [
            [
                "b0c8612f.988fb",
                "68cc8ea0.2803d8"
            ]
        ]
    },
    {
        "id": "68cc8ea0.2803d8",
        "type": "change",
        "z": "9948466a.d6ebc8",
        "name": "broadcast timestamp",
        "rules": [
            {
                "t": "set",
                "p": "timestamp",
                "pt": "global",
                "to": "",
                "tot": "date"
            }
        ],
        "action": "",
        "property": "",
        "from": "",
        "to": "",
        "reg": false,
        "x": 340,
        "y": 220,
        "wires": [
            []
        ]
    },
    {
        "id": "88236e.6131d49",
        "type": "function",
        "z": "9948466a.d6ebc8",
        "name": "Flash update",
        "func": "var newmsg={};\n\nif (msg.topic==\"init\") //not real value\n{\n     newmsg.payload= \"none\" ;\n\treturn newmsg;\n\n}\nmsg.payload=msg.payload[0];\n\n\tnewmsg = {\n    payload: \"orange\" \n\t};\n\treturn newmsg;\n",
        "outputs": 1,
        "noerr": 0,
        "x": 670,
        "y": 240,
        "wires": [
            [
                "4b787148.ac93c8"
            ]
        ]
    },
    {
        "id": "4b787148.ac93c8",
        "type": "link out",
        "z": "9948466a.d6ebc8",
        "name": "Flash",
        "links": [
            "bfc148b7.c56998"
        ],
        "x": 870,
        "y": 260,
        "wires": [],
        "icon": "node-red/arrow-in.svg",
        "l": true
    },
    {
        "id": "1fb67b7.0337e05",
        "type": "delay",
        "z": "9948466a.d6ebc8",
        "name": "",
        "pauseType": "delay",
        "timeout": "1",
        "timeoutUnits": "seconds",
        "rate": "1",
        "nbRateUnits": "1",
        "rateUnits": "second",
        "randomFirst": "1",
        "randomLast": "5",
        "randomUnits": "seconds",
        "drop": false,
        "x": 840,
        "y": 200,
        "wires": [
            [
                "cea00b37.2f68a8"
            ]
        ]
    },
    {
        "id": "10b18295.20351d",
        "type": "function",
        "z": "9948466a.d6ebc8",
        "name": "Flash update",
        "func": "var newmsg={};\n\nif (msg.topic==\"init\") //not real value\n{\n     newmsg.payload= \"none\" ;\n\treturn newmsg;\n\n}\nmsg.payload=msg.payload[0];\n\n\tnewmsg = {\n    payload: \"yellow\" \n\t};\n\treturn newmsg;\n",
        "outputs": 1,
        "noerr": 0,
        "x": 650,
        "y": 280,
        "wires": [
            [
                "4b787148.ac93c8"
            ]
        ]
    },
    {
        "id": "feac783d.28bd78",
        "type": "ui_artlessgauge",
        "z": "9948466a.d6ebc8",
        "group": "b428ba8.81494c8",
        "order": 5,
        "width": 4,
        "height": 4,
        "name": "eco2",
        "icon": "",
        "label": "eCO2",
        "unit": "ppm",
        "layout": "radial",
        "decimals": "0",
        "differential": false,
        "minmax": true,
        "colorTrack": "#555555",
        "colorFromTheme": true,
        "property": "payload.data[0]fields.eco2",
        "sectors": [
            {
                "val": 400,
                "col": "#23ff38",
                "t": "min",
                "dot": 0
            },
            {
                "val": 600,
                "col": "#2dcc35",
                "t": "sec",
                "dot": 0
            },
            {
                "val": 700,
                "col": "#deff58",
                "t": "sec",
                "dot": 0
            },
            {
                "val": 800,
                "col": "#e5ae4c",
                "t": "sec",
                "dot": 0
            },
            {
                "val": 1200,
                "col": "#ff66a0",
                "t": "sec",
                "dot": 0
            },
            {
                "val": 1500,
                "col": "#cc2d9c",
                "t": "sec",
                "dot": 0
            },
            {
                "val": 2000,
                "col": "#cc2d9c",
                "t": "max",
                "dot": 0
            }
        ],
        "lineWidth": 3,
        "bgcolorFromTheme": true,
        "diffCenter": "",
        "x": 870,
        "y": 620,
        "wires": []
    },
    {
        "id": "851f0d3c.a6e6c8",
        "type": "ui_artlessgauge",
        "z": "9948466a.d6ebc8",
        "group": "b428ba8.81494c8",
        "order": 4,
        "width": 4,
        "height": 4,
        "name": "etvoc",
        "icon": "",
        "label": "tvoc",
        "unit": "bbp",
        "layout": "radial",
        "decimals": "0",
        "differential": false,
        "minmax": true,
        "colorTrack": "#555555",
        "colorFromTheme": true,
        "property": "payload.data[0]fields.tvoc",
        "sectors": [
            {
                "val": 0,
                "col": "#23ff38",
                "t": "min",
                "dot": 0
            },
            {
                "val": 300,
                "col": "#a5ff83",
                "t": "sec",
                "dot": 0
            },
            {
                "val": 600,
                "col": "#e7ff5a",
                "t": "sec",
                "dot": 0
            },
            {
                "val": 900,
                "col": "#e59f20",
                "t": "sec",
                "dot": 0
            },
            {
                "val": 1200,
                "col": "#cc240b",
                "t": "sec",
                "dot": 0
            },
            {
                "val": 2000,
                "col": "#cc240b",
                "t": "max",
                "dot": 0
            }
        ],
        "lineWidth": 3,
        "bgcolorFromTheme": true,
        "diffCenter": "",
        "x": 870,
        "y": 660,
        "wires": []
    },
    {
        "id": "9bd7774.8100988",
        "type": "modbus-getter",
        "z": "9948466a.d6ebc8",
        "name": "RiG-ePlus @ Location",
        "showStatusActivities": true,
        "showErrors": true,
        "logIOActivities": false,
        "unitid": "160",
        "dataType": "InputRegister",
        "adr": "0",
        "quantity": "8",
        "server": "b24a2e1a.d69b",
        "useIOFile": false,
        "ioFile": "",
        "useIOForPayload": false,
        "emptyMsgOnFail": false,
        "keepMsgProperties": false,
        "x": 320,
        "y": 500,
        "wires": [
            [
                "61819ea4.2d53",
                "8ccfd01.a10a3b",
                "18ae0958.755467"
            ],
            []
        ]
    },
    {
        "id": "cea00b37.2f68a8",
        "type": "link out",
        "z": "9948466a.d6ebc8",
        "name": "Broadcast",
        "links": [
            "142d0245.78fc5e"
        ],
        "x": 1010,
        "y": 200,
        "wires": [],
        "icon": "node-red/trigger.svg",
        "l": true
    },
    {
        "id": "142d0245.78fc5e",
        "type": "link in",
        "z": "9948466a.d6ebc8",
        "name": "Broadcast_Start",
        "links": [
            "cea00b37.2f68a8"
        ],
        "x": 160,
        "y": 440,
        "wires": [
            [
                "9bd7774.8100988"
            ]
        ],
        "icon": "node-red/trigger.svg",
        "l": true
    },
    {
        "id": "b428ba8.81494c8",
        "type": "ui_group",
        "z": "",
        "name": "TEMP",
        "tab": "99d58c10.2cbeb8",
        "order": 2,
        "disp": true,
        "width": 12,
        "collapse": false,
        "disabled": false,
        "hidden": false
    },
    {
        "id": "b24a2e1a.d69b",
        "type": "modbus-client",
        "z": "",
        "name": "mbusd-1",
        "clienttype": "tcp",
        "bufferCommands": true,
        "stateLogEnabled": false,
        "queueLogEnabled": false,
        "tcpHost": "192.168.1.51",
        "tcpPort": "502",
        "tcpType": "DEFAULT",
        "serialPort": "/dev/ttyUSB",
        "serialType": "RTU-BUFFERD",
        "serialBaudrate": "9600",
        "serialDatabits": "8",
        "serialStopbits": "1",
        "serialParity": "none",
        "serialConnectionDelay": "100",
        "unit_id": 160,
        "commandDelay": 200,
        "clientTimeout": 3000,
        "reconnectOnTimeout": true,
        "reconnectTimeout": 3000,
        "parallelUnitIdsAllowed": false
    },
    {
        "id": "99d58c10.2cbeb8",
        "type": "ui_tab",
        "z": "",
        "name": "TEMP",
        "icon": "dashboard",
        "order": 4,
        "disabled": false,
        "hidden": false
    }
]
```