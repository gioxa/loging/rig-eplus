/**
 *  RS485 Modbus for ePlus
 *
 * BME280 + CCS811 => THP + CO2-TVOC+R
 *
 */

#include <ModbusSlave.h>
#include <logit_utils.h>

#include <Wire.h>    // I2C library
#include <ccs811.h>  // CCS811 library

#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>


#define MBUS_SERIAL_BAUDRATE 19200
#define MBUS_SERIAL_PORT Serial   // Serial port to use for RS485 communication, change to the port you're using.
#define MBUS_RS485_CTRL_PIN -1

#define CO2WAKE_PIN 4


// LED PIN definitions
#define RiG_LED_GREEN 2
#define RiG_LED_RED 3


#define MBUS_BASE_ADDRES 128
#define MBUS_MAX_BASE_ADDRES 199

Modbus mbus_slave(MBUS_SERIAL_PORT, MBUS_BASE_ADDRES, MBUS_RS485_CTRL_PIN);


#define MOD_REG_TEMPERATURE 0
#define MOD_REG_HUMIDITY 1
#define MOD_REG_PRESURE 2
#define MOD_REG_CO2 4
#define MOD_REG_TVOC 5
#define MOD_REG_CCS811_STATUS 6
#define MOD_REG_STATUS 7

#define MOD_REG_LAST MOD_REG_STATUS+1

// MOD_REG_STATUS bit definitions
#define MOD_STATUS_IDLE 0
#define MOD_STATUS_RDY 1
#define MOD_STATUS_ERROR 2




#define BME_CS 10
#define SEALEVELPRESSURE_HPA (1013.25)

Adafruit_BME280 bme(BME_CS); // hardware SPI

CCS811 ccs811(CO2WAKE_PIN);

struct mavg{
  uint16_t ar[4];
  uint8_t ptr;
};


struct mavg eco2_a;
struct mavg etvoc_a;

boolean start_measure;
uint16_t measure_broadcast;

unsigned long endTime;
boolean have_co2;

void init_av(struct mavg * s, uint16_t d)
{
  int i;
  for (i=0;i<4;i++)
  {
    s->ar[i]=d;
  }
  s->ptr=0;
}
uint16_t calc_movingAvg(struct mavg * s,uint16_t new_value)
{
  s->ar[(s->ptr & 3)]=new_value&0x3fff;
  s->ptr++;
  uint16_t av=(s->ar[0]+s->ar[1]+s->ar[2]+s->ar[3]) >>2;
  return(av);
}
// ModBus Data helpers


void convert_float_to_modbus(float value,uint16_t *ibuf,int regn)
{
	uint32_t *rval =(uint32_t *)&value;//*reinterpret_cast<uint32_t*>(&value);
	ibuf[regn] = (*rval & 0x0ffff0000 )>> 16;
	ibuf[regn+1]=*rval & 0x0000ffff;
}

uint16_t convert_float_to_TCx100(float value)
{
   // float to fixed decimal integer
  int t=value*100;
  return ((uint16_t)t);
  // int to modbus
  //if (value<0)
  //  return(65536+t);
  //else
  //  return(t);
}

//MPDBUS Register Constants
#define MOD_REG_NR MOD_REG_LAST

// data array for modbus network sharing
uint16_t au16data[MOD_REG_NR];


int ledState = LOW;
unsigned long previousMillis = 0;        // will store last time LED was updated
const long interval = 400;

void blink_led()
{
 unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval)
  {
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(RiG_LED_RED, ledState);
  }
}



uint8_t write_coils(uint8_t fc, uint16_t address, uint16_t length)
{
    if (fc==5 && address==229 && length==1)
    {
      if (mod_control_iddle())
      {
          // Tell BME280 to begin measurement.
          broadcast(BROADCAST_30s);
          //measure();
      }
    }
    else
    {
      // adress out of range
      return STATUS_ILLEGAL_DATA_ADDRESS;
    }
    return STATUS_OK;
}



// Handle the function code Read Input Registers (FC=04) and write back the values from au16data[16] (input registers).
uint8_t read_values(uint8_t fc, uint16_t address, uint16_t length)
{
    //if (!mod_control_iddle())  return STATUS_SLAVE_DEVICE_BUSY;
    if (inrange(address,length,0,MOD_REG_LAST-1) )
    {
      // Read the analog inputs
      for (int i = 0; i < length; i++)
      {
          // Write the state of the analog pin to the response buffer.
          mbus_slave.writeRegisterToBuffer(i, au16data[address + i]);
      }
      clr_mod_control_rdy();
    }
    else
    {
      // adress out of range
      return STATUS_ILLEGAL_DATA_ADDRESS;
    }
    return STATUS_OK;
}

// Handle the function code Read Holding Registers (FC=03).
uint8_t read_direct_values(uint8_t fc, uint16_t address, uint16_t length)
{
  if (!mod_control_iddle())  return STATUS_SLAVE_DEVICE_BUSY;
  if  (inrange(address,length,9191,9191))
  {   // test range read, returns 9191 from adress 9191
      mbus_slave.writeRegisterToBuffer(0, 9191);
      return STATUS_OK;
  }
  else if  (inrange(address,length,1000,1000))
  {
    //Get temp offset from EEPROM (logutils)
    //float f= bme.getTemperatureCompensation();
    float f=read_offset();
    mbus_slave.writeRegisterToBuffer(0,convert_float_to_TCx100(f));
    return STATUS_OK;
  }
  else if  (inrange(address,length,1001,1001))
  {
    uint16_t h = read_have();
    mbus_slave.writeRegisterToBuffer(0,h);
    return STATUS_OK;
  }
  else
  {
      /* Address out of range*/
      return STATUS_ILLEGAL_DATA_ADDRESS;
  }
}

// Handle the function code write Holding Registers (FC=06).
uint8_t write_direct_values(uint8_t fc, uint16_t address, uint16_t length)
{
  if (!mod_control_iddle())  return STATUS_SLAVE_DEVICE_BUSY;

 if  (inrange(address,length,9191,9191))
  { // change UnitId
    uint8_t address=(uint8_t)(mbus_slave.readRegisterFromBuffer(0)& 0x00ff);
    // check if in our device category range
    if (address==set_unitId(address))
    {
      set_soft_reset();
      return (STATUS_OK);
    }
    else
    {
      return(STATUS_ILLEGAL_DATA_VALUE);
    }
  }
  else if  (inrange(address,length,9391,9391))
  { // new broadcast
     uint16_t broadcast_v;
     broadcast_v=mbus_slave.readRegisterFromBuffer(0);
    if (broadcast_v & BROADCAST_SAVE_BASELINE)
    {
       uint16_t baseline;
       ccs811.get_baseline(baseline);

    }
    if (broadcast_v & BROADCAST_RESTORE_BASELINE)
    {
       uint16_t baseline;

       ccs811.set_baseline(baseline);
       
    }
    if (broadcast_v & BROADCAST_SOFT_RESET)
    {
       set_soft_reset();
    }
    if (broadcast_v & BROADCAST_30s_UP)
    {
        start_measure=true;
        measure_broadcast=value&& BROADCAST_30s_UP;
    }
    return STATUS_OK;
  }

  else if  (inrange(address,length,1000,1000))
  {
    //Get and set temp offset
    //int writeoffset(float offset_value);
    float f= mbus_slave.readRegisterFromBuffer(0)/100.0;
    writeoffset(f);
    bme.setTemperatureCompensation(read_offset());
    //bme.setTemperatureCompensation(f);
    return STATUS_OK;
  }
  else if  (inrange(address,length,1001,1001))
  {
    //Get and set temp have
     uint8_t h= mbus_slave.readRegisterFromBuffer(0);
     write_have(h);
     set_soft_reset();
     return STATUS_OK;
  }
  else
  {
    /* address out of range */
     return STATUS_ILLEGAL_DATA_ADDRESS;
  }
}

void setup_mbus()
{
  mbus_slave.cbVector[CB_WRITE_COILS] = write_coils;
  mbus_slave.cbVector[CB_READ_INPUT_REGISTERS] = read_values;
  mbus_slave.cbVector[CB_WRITE_HOLDING_REGISTERS] = write_direct_values;
  mbus_slave.cbVector[CB_READ_HOLDING_REGISTERS] = read_direct_values;
  //mbus
  // set correct slave addres
  mbus_slave.setUnitAddress( get_unitId());
  // Set the serial port and slave to the given baudrate.
  MBUS_SERIAL_PORT.begin(MBUS_SERIAL_BAUDRATE);
  mbus_slave.begin(MBUS_SERIAL_BAUDRATE);
}


void setup() {
  // clr registers
  int8_t i;
  for (i=0;i<MOD_REG_NR;i++)
  {
    au16data[i]=0;
  }
  // setup logutils
  setup_log_utils(&au16data[MOD_REG_STATUS],-1,-1,MBUS_BASE_ADDRES, MBUS_MAX_BASE_ADDRES  );
  //force_unitId(MBUS_BASE_ADDRES);

  setup_mbus();

  // check if connected and working
  endTime=bme.begin();
  if (!endTime)
  {
    set_mod_control_iddle();
    set_mod_control_error();
    clr_mod_control_rdy();
    while (1)
    {
          delay(10);
          endTime=bme.begin();
          if (endTime) break;
          mbus_slave.poll();
          blink_led();
    }
  }
  bme.setTemperatureCompensation(read_offset());
   bme.setSampling(Adafruit_BME280::MODE_FORCED,
                        Adafruit_BME280::SAMPLING_X2, // temperature
                        Adafruit_BME280::SAMPLING_X2, // pressure
                        Adafruit_BME280::SAMPLING_X1, // humidity
                        Adafruit_BME280::FILTER_X4   );

  if ( (read_have() & HAVE_CO2) == HAVE_CO2)
  {
    have_co2=true;
    // Enable I2C
    Wire.begin();
    ccs811.begin();
    ccs811.start(CCS811_MODE_1SEC);

    init_av(&eco2_a,400);
    init_av(&etvoc_a,0);
  }
  au16data[MOD_REG_CCS811_STATUS ]= 0xffff;
  set_mod_control_iddle();
  clr_mod_control_error();
  clr_mod_control_rdy();

  start_measure=true;
  measure_broadcast=BROADCAST_30s_UP;
}

void measure_it()
{
  if (start_measure)
  {
    if (measure_broadcast & BROADCAST_30s_UP)
    {
      uint16_t eco2, etvoc, errstat, raw;
      float t,h,p;
      clr_mod_control_iddle();
      clr_mod_control_rdy();
      bme.takeForcedMeasurement(); // has no effect in normal mode
      t=bme.readTemperature();
      h=bme.readHumidity();
      p=bme.readPressure();
      if ( t!=NAN && h!=NAN)
      {
          au16data[MOD_REG_TEMPERATURE]=int(t*100);//*reinterpret_cast<uint16_t*>(&value);
          au16data[MOD_REG_HUMIDITY]=int(h*100);//*reinterpret_cast<uint16_t*>(&value);
          convert_float_to_modbus( p , au16data, MOD_REG_PRESURE);
      }
      if (have_co2 && t!=NAN && h!=NAN )
      {
        ccs811.set_envdata_Celsius_percRH(t,h);
        ccs811.read(&eco2,&etvoc,&errstat,&raw);
        if( errstat==CCS811_ERRSTAT_OK )
        {
          au16data[MOD_REG_CO2]=calc_movingAvg(&eco2_a,eco2);
          au16data[MOD_REG_TVOC]=calc_movingAvg(&etvoc_a,etvoc);
          //convert_float_to_modbus(1612.903*(raw%1024)/(raw/1024),au16data,MOD_REG_RESISTANCE); // (1650*1000L/1023)*=1612.90323
          clr_mod_control_error();
        }
        else if( errstat==CCS811_ERRSTAT_OK_NODATA )
        {
          au16data[MOD_REG_CO2]=0;
          au16data[MOD_REG_TVOC]=0;

          set_mod_control_error();
          au16data[MOD_REG_CCS811_STATUS ]= errstat;
        }
      }
      else
      {
        au16data[MOD_REG_CCS811_STATUS ]= 0xffff;
        clr_mod_control_error();
      }
      set_mod_control_iddle();
      set_mod_control_rdy();
      start_measure=false;
    }

    
  }
}

// main loop
void loop()
{
  blink_led();
  mbus_slave.poll();
  if (SoftResetFlag())
  { // if soft reset flag is set, do an extra modbuspoll to answer OK
    for (int i=1;i<15;i++)
    {
      mbus_slave.poll();
      delay(50);
    }
  }
  SoftResetCheck();
  measure_it();
}
